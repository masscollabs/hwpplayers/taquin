/*
 * Copyright (C) 2020  Aloys Liska
 * 
 * This file is part of Taquin.
 * 
 * Taquin is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; version 3.
 *
 * Taquin is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Taquin.  If not, see <http://www.gnu.org/licenses/>.
 */

import QtQuick 2.7
import Ubuntu.Components 1.3
import "taquin.js" as Taquin

Page {
    id: settingsPage
    visible: false
    
    property var matrixSizeValues: [2,3,4,5,6,7,8,9,10]    // range of values for maximum number of rows or columns
    property int param_maxRow: 0;      // user selected value to be set for maxRow. Zero means parameter is unchanged
    property int param_maxCol: 0;      // user selected value to be set for maxCol. Zero means parameter is unchanged
    
    property var imageSelecTableText: [i18n.tr("Numbers"),i18n.tr("Flower"),i18n.tr("Mountain"),i18n.tr("Rainbow")]
    property var imageSelecTable: [0,1,2,3]

    
    Component.onDestruction: {  
        // if settings were changed, apply them.
        
        if (param_maxRow != 0) {
            maxRow = param_maxRow 
        }   
        if (param_maxCol != 0) {
            maxCol = param_maxCol
        }    
        if ((param_maxRow != 0) || (param_maxCol != 0)) { 
            Taquin.newGame();
        }
    }
    
    header: PageHeader {
        id: headerSettings
        title: i18n.tr('Settings')
        
        StyleHints {
            foregroundColor: UbuntuColors.orange
            backgroundColor: UbuntuColors.porcelain
            dividerColor: UbuntuColors.slate
        }
    }

    // TODO : add a flickable?
    
    Column {
        id: topcolumn
        anchors.top: headerSettings.bottom
        anchors.left: parent.left
        width:parent.width
        topPadding: units.gu(2)
        
        // Tiles display
        ListItem {
            Grid {
                anchors.left: parent.left
                anchors.leftMargin: units.gu(4)
                columns: 2
                spacing: units.gu(2)
                topPadding: units.gu(2)
                
                CheckBox {
                    checked: squareTile
                    onClicked: squareTile = checked
                }
                Label {
                    text: i18n.tr('Display square tiles')
                }
            }
        }
        
        // Images selection  
        ListItem {
            height: 2*units.gu(2) + imgSelOptionSelector.height
            
            Column {
                anchors.left: parent.left
                anchors.leftMargin: units.gu(4)
                width: parent.width/2
                topPadding: units.gu(2)
                
                OptionSelector {
                    id: imgSelOptionSelector
                    text: i18n.tr("<b>Images</b>")
                    model: imageSelecTableText
                    selectedIndex: imageSelecTable.indexOf(indexImg)
                    containerHeight: itemHeight * 4     // to display 4 elements, other elements accessible by scrolling

                    onDelegateClicked:  {
                        indexImg = imageSelecTable[index];
                    }
                }
            }
        }
        
        // Rows and columns
        ListItem {
            height: 6*rcparamtext.height + rowOptionSelector.height + colOptionSelector.height 
            Column {
                id: rcCol
                anchors.left: parent.left
                anchors.leftMargin: units.gu(4)
                width: parent.width/2
                spacing: units.gu(2)
                topPadding: units.gu(2)
                
                Label {
                    id: rcparamtext
                    width: topcolumn.width - 2*rcCol.anchors.leftMargin // width set to have appropriate text wrapping
                    wrapMode: Text.Wrap
                    text: i18n.tr("<i>Changing these parameters will re-initialize the game.</i>")
                } 
                
                OptionSelector {
                    id: rowOptionSelector
                    text: i18n.tr("<b>Rows</b>")   
                    model: matrixSizeValues
                    selectedIndex: matrixSizeValues.indexOf(maxRow)
                    containerHeight: itemHeight * 4     // to display 4 elements, other elements accessible by scrolling

                    onDelegateClicked:  {
                        param_maxRow = matrixSizeValues[index];
                        // reset the value if it is unchanged: value zero means parameter unchanged
                        if (param_maxRow == maxRow) {
                            param_maxRow = 0
                        }
                    }
                }
                
                OptionSelector {
                    id: colOptionSelector
                    text: i18n.tr("<b>Columns</b>")
                    model: matrixSizeValues
                    selectedIndex: matrixSizeValues.indexOf(maxCol)
                    containerHeight: itemHeight * 4     // to display 4 elements, other elements accessible by scrolling

                    onDelegateClicked:  {
                        param_maxCol = matrixSizeValues[index];
                        // reset the value if it is unchanged: value zero means parameter unchanged
                        if (param_maxCol == maxCol) {
                            param_maxCol = 0
                        }
                    }
                }
            }
        }
    }
}
