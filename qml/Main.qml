/*
 * Copyright (C) 2020  Aloys Liska
 * 
 * This file is part of Taquin.
 * 
 * Taquin is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; version 3.
 *
 * Taquin is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Taquin.  If not, see <http://www.gnu.org/licenses/>.
 */

//  TODO version 2:
//   - provide user choice of the image from its gallery and save it in data from taquin app
//   - use taphandler instead of click? and use Ubuntu.Components SwipeArea?
//   - add a sound when moving a tile?


import QtQuick 2.7
import Ubuntu.Components 1.3
import QtQuick.Layouts 1.3
import Qt.labs.settings 1.0
import Qt.labs.platform 1.0
import "taquin.js" as Taquin


MainView {
    id: mainView
    objectName: 'mainView'
    applicationName: 'taquin.aloysliska'
    automaticOrientation: true

    width: units.gu(50)
    height: units.gu(75)
    
    /*************/
    /* variables */
    /*************/

    /* The puzzle is composed of tiles arranged in rows and columns.
     * Mathematically, the position of these tiles are represented with a matrix or a table
     * In the code, this matrix (or table) is stored as a one dimensional array named tileTable
     */
    property int maxRow: 4;                 // number of rows initialized to defaut value
    property int maxCol: 4;                 // number of columns initialized to default value
    property int maxRowIndex: maxRow - 1;   // maximum index for row
    property int maxColIndex: maxCol - 1;   // maximum index for column
    property int tabTsize: maxRow*maxCol;   // number of element in the matrix (or in the table)

    // Tile geometry : square or rectangle
    property bool squareTile: true
    
    // Index of selected image. Value zero is for no image but numbers
    property int indexImg: 0 
        
    // indicator of game over
    property bool gameOver: false
    
    // matrix definition
    property var tileTable: Taquin.createMatrix(maxRow,maxCol)

    // counting of move
    property int movcount: settings.savedmovcount

    Settings { 
        // note: it is important to define maxRow as a int, and not a var, otherwise when settings are restored, type is a string which will cause type error
        id: settings
        property alias maxRow: mainView.maxRow
        property alias maxCol: mainView.maxCol
        property alias squareTile: mainView.squareTile
        property alias indexImg: mainView.indexImg
        property alias gameOver: mainView.gameOver
        property var savedtileTable: []
        property int savedmovcount: 0
    }
    
    // definition of the free tile: this is an array [i,j] which are indexes in the matrix
    property var currFreeTile: [0,0]
    property int initFreeIndex: 0  // index of free tile for access in the repeater of the tiles
 
    property var temp: 0
 
 
    /******************/
    /* The PageStack  */
    /******************/

    PageStack {
        id: pageStack
        Component.onCompleted: {
            push(mainPage)
        }
        
        Page {
            id: mainPage
            visible: false
            
            header: PageHeader {
                id: headerMainPage
                title: i18n.tr('Taquin')
                
                StyleHints {
                    foregroundColor: UbuntuColors.orange
                    backgroundColor: UbuntuColors.porcelain
                    dividerColor: UbuntuColors.slate
                }
                
                contents: Item { 
                    Label {
                        id: labelTitle
                        text: i18n.tr('Taquin')
                        color: UbuntuColors.orange
                        textSize: Label.XLarge
                        anchors.verticalCenter: parent.verticalCenter
                        anchors.left: parent.left
                        anchors.leftMargin: units.gu(2)
                    }
                    Label {
                        text: i18n.tr('moves: ') + movcount
                        color: UbuntuColors.slate
                        anchors.verticalCenter: parent.verticalCenter
                        anchors.right: parent.right
                        anchors.rightMargin: units.gu(4)
                    }
                    anchors.fill: parent
                }
              
                trailingActionBar.actions: [
                    Action {
                        iconName: "info"
                        text: i18n.tr('Information')
                        onTriggered: {
                            pageStack.push(Qt.resolvedUrl("InfoPage.qml"))
                        }  
                    },
                    Action {
                        iconName: "settings"
                        text: i18n.tr('Settings')
                        onTriggered: {
                            pageStack.push(Qt.resolvedUrl("SettingsPage.qml"))
                        }
                    },                    
                    Action {
                        iconName: "reload"
                        text: i18n.tr('New game')
                        onTriggered: {
                            Taquin.newGame();
                        }   
                    }
                ]
            }
            
            Item {
                id:itemTable
                property var imageHeight: parent.height - headerMainPage.height
                
                anchors.horizontalCenter: parent.horizontalCenter
                anchors.top: headerMainPage.bottom
                anchors.topMargin: {
                    if ((squareTile == true) && (imageHeight > parent.width))
                        return ((imageHeight - parent.width)/2);
                    else
                        return 0;
                }
               
                height: { 
                    if (squareTile == true) {
                        if (imageHeight > parent.width)
                            return parent.width;
                        else
                            return imageHeight;
                    }
                    else
                        return imageHeight;
                }
                width:  {
                    if (squareTile == true) {
                        if (imageHeight > parent.width)
                            return parent.width;
                        else
                            return imageHeight;
                    }
                    else
                        return parent.width;
                } 
                
                // The core of taquin: the matrix of tiles
                Repeater {
                    id: mouleAgaufre
                    model: { 
                        //console.log("model=", tabTsize)     
                        /* FIXME we can see that when starting the game there is three logs. 
                        *  This is probably due to bindings. No consequences but not clean.
                        *  tabTsize depends on maxRow and maxCol. So when changing both (from startup or from SettingsPage)
                        *  this cause two updates of mouleAgaufre, but one update would be enough.
                        */
                        return tabTsize;
                    }
                    Tile { 
                        n: index
                        tileVal: tileTable[index]
                        
                        onClicked: { 
                            temp = n
                            movTile = Taquin.moveSelectedTile(coord)
                            if ((movTile[0] != 0) || (movTile[1] != 0)) {
                                mouleAgaufre.itemAt(initFreeIndex).n = temp     // to move the free tile
                            }
                            gameOver = Taquin.checkForCompleted()
                            if (gameOver)
                                mouleAgaufre.itemAt(initFreeIndex).visible = true   // make visible the free tile
                        }
                    }
                }
                
                Component.onCompleted: {
                    // Initialize tileTable from saved settings or if they do not exist, create a new one
                    temp = settings.savedtileTable.slice()
                                     
                    if (temp.length != 0) {
                        for (var i = 0; i < tabTsize; i++) {
                            // savedtileTable is an array of string, so convert each element to integer 
                            temp[i] = parseInt(temp[i],10)
                        }
                        tileTable = temp.slice() 
                        
                    }
                    else {
                        tileTable = Taquin.createMatrix(maxRow,maxCol)
                    }
                    
                    currFreeTile = Taquin.initFreeTile()
                }
            }
            
            Component.onDestruction: {
                settings.savedtileTable = tileTable.slice()
                settings.savedmovcount = movcount
            }
        }
    }
}
