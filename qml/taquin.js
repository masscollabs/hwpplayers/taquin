/*
 * Copyright (C) 2020  Aloys Liska
 * 
 * This file is part of Taquin.
 * 
 * Taquin is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; version 3.
 *
 * Taquin is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Taquin.  If not, see <http://www.gnu.org/licenses/>.
 */


/*
 * createMatrix(n,m)
 * 1) create the matrix of n lines and m columns with random values 
 *    in fact this matrix is created in code as a one dimensional array
 * 2) check the parity of the permutation
 * 3) change the parity if necessary in order to have a solvable game
 */
function createMatrix(n,m) {
    var indexMax = n*m - 1
    var matrixInit = [];
    var matrixOut = [];
    var x;
    var y;    
    var z;
    
    // 1) create matrixInit containing numbers ordered from 0 to indexMax
    for (var i = 0; i < n; i++) {
        for (var j = 0; j < m; j++) {
            matrixInit.push(j + i*m);
        }
    }

    // mixup elements of matrixInit to create a matrixOut which contains numbers in random order 
    for (var k = indexMax; k >= 0; k--) {
        x = Math.floor((k + 1)*Math.random()); // random integer value between 0 and k
        y = matrixInit.splice(x,1); // remove from matrixInit (which contains k elements) the element at random index x
        matrixOut.push(y[0]);       // add the element from matrixInit in matrixOut (at the end posiion)
    }
    
    // 2) Check parity of permutation
    // x: count the number of inversion in matrixOut
    x = 0;
    for (var k = 0; k < indexMax; k++) {
        for (var p = k + 1; p <= indexMax; p++) {
            if (matrixOut[p] < matrixOut[k])
                x++;
        }
    }
    
    // y: count the number of moves for the free tile to be in last position (maxRowIndex,maxColIndex)
    z = matrixOut.indexOf(indexMax);
    var zi = Math.floor(z/maxCol);
    var zj = z%maxCol;
    y = (maxRowIndex - zi) + (maxColIndex - zj);    
    
    // compare parity of x and y
    if (x%2 != y%2) {
        // 3)  this game is not solvable
        // change the parity to make solvable the game
        for (var k = 0; k <= indexMax; k++) {
            if ((matrixOut[k+1] != indexMax) && (matrixOut[k] != indexMax)) {
                // perform the permutation
                z = matrixOut[k+1];
                matrixOut[k+1] = matrixOut[k];
                matrixOut[k] = z;
                break;
            }
        }        
    }
    
    gameOver = false;
    
    console.log("New Matrix: ", matrixOut, " n:", n," m:", m);
    
    return matrixOut;
}


/*
 * initFreeTile()
 * return the position in the matrix of the free tile 
 */
function initFreeTile() {
    var z;
    var val = [0,0];
    
    z = tileTable.indexOf(tabTsize - 1);
    val[0] = Math.floor(z/maxCol);
    val[1] = z%maxCol;
    
    initFreeIndex = z;
    
    return val;
}


/*
 * newGame()
 * 1) create matrix with random configuration
 * 2) Set the related free tile position
 * 3) re-initialize all the tiles
 * 4) re-initialize the counting of moves
 */
function newGame() {
    gameOver = false;
    
    // 1) create tileTable
    tileTable=createMatrix(maxRow,maxCol);
    
    // 2) set free tile position
    currFreeTile = initFreeTile()
    
    // 3) re-initialize all the tiles
    for (var k = 0; k < tabTsize; k++) {
        mouleAgaufre.itemAt(k).n = k;
        mouleAgaufre.itemAt(k).visible = true
    }
    mouleAgaufre.itemAt(initFreeIndex).visible = false
    
    // 4) re-initialize the counting of moves
    movcount = 0;
}
    
/*
 * moveSelectedTile(cell))
 * return the movement to be done, it can be no move
 */
function moveSelectedTile(cell) {
    var mov;
    var pos;
    var freePos;
    var temp;
 
    if (gameOver) {
        mov = [0,0];    // forbid the moves when game is over
    }
    else {
        
        if ((cell[0] == currFreeTile[0]+1) && (cell[1] == currFreeTile[1])) {
            mov = [-1,0];
            movcount++;
        }
        else if ((cell[0] == currFreeTile[0]-1) && (cell[1] == currFreeTile[1])) {
            mov = [1,0];
            movcount++;
        }
        else if ((cell[0] == currFreeTile[0]) && (cell[1] == currFreeTile[1]-1)) {
            mov = [0,1];
            movcount++;
        }
        else if ((cell[0] == currFreeTile[0]) && (cell[1] == currFreeTile[1]+1)) {
            mov = [0,-1];
            movcount++;
        }
        else {
            mov = [0,0];
        }
        
        if ((mov[0] != 0) || (mov[1] != 0)) {
            
            pos = maxCol*cell[0] + cell[1];
            freePos = maxCol*currFreeTile[0] + currFreeTile[1];
            
            // permutation in the table
            temp = tileTable[pos];
            tileTable[pos] = tileTable[freePos];
            tileTable[freePos] = temp;
            
            // update the free tile
            currFreeTile[0] = currFreeTile[0] - mov[0];
            currFreeTile[1] = currFreeTile[1] - mov[1];
        }
    }
    
    return mov;
}


/* checkForCompleted()
 * check if all tiles are re-organized in correct order
 */
function checkForCompleted() {
    for (var pos = 0; pos < tabTsize; pos++) {
        if (tileTable[pos]!=pos) {
            return false;
        }
    }
    
    console.log("YOU WIN: ", tileTable);
    
    return true;
}
